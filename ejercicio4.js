//Pirámide con un solo for
function piramide() {
    for (var i = 1; i < 10; i++) {
        //Convertimos i en String para poder utilizar .repeat()
        console.log(i.toString().repeat(i))
    }
  }
  
  piramide();


/*

//Pirámide con for anidado

function piramide2() {
    for (var i = 1; i <= 9; i++) {
        var s = "";
        for (var j = 0; j<i; j++) {
            s += i;
         }
    console.log(s);
    }
}

piramide2();

*/